//
// Operand.hh for  in /home/fritsc_h/projets/cpp_abstractvm
//
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
//
// Started on  Thu Feb 27 18:02:11 2014 Fritsch harold
// Last update Sat Mar  1 15:19:55 2014 Jean Gravier
//

#ifndef OPERAND_HH_
# define OPERAND_HH_
# include <string>
# include <sstream>
# include <iomanip>
# include <cmath>
# include <limits>
# include "IOperand.hh"
# include "IO.hh"
# include "CPU.hh"
# include "VMException.hh"

template<typename T>
class				Operand: public IOperand
  {
    public:
    Operand(std::string const& stringValue, eOperandType type, int precision): _stringValue(stringValue), _type(type), _precision(precision)
      {
	std::stringstream ss(stringValue);

	if (type != INT8)
	  ss >> _value;
	else
	  {
	    int value;

	    ss >> value;
	    _value = value;
	    if (value > std::numeric_limits<T>::max())
	      throw VMException("overflow");
	    else if (value < std::numeric_limits<T>::min())
	      throw VMException("underflow");
	  }
      }
    ~Operand() {}

    std::string const&		toString() const { return (_stringValue); }
    T				getValue() const { return (_value); }
    int				getPrecision() const { return (_precision); }
    eOperandType		getType() const { return (_type); }

    IOperand			*operator+(IOperand const& operand) const
      {
	T 			value = 0;
	double 			temp_val = 0;
	int			precision = 0;
	eOperandType		type;
	std::string 		stringValue;
	std::stringstream	ss(operand.toString());

	if (operand.getType() != INT8)
	  ss >> value;
	else
	  {
	    ss >> temp_val;
	    value = temp_val;
	  }
	temp_val = _value + value;
	type = operand.getType();
	if (_precision < operand.getPrecision())
	  {
	    precision = operand.getPrecision();
	    type = operand.getType();
	  }

	ss.clear();
	ss << temp_val;
	stringValue = ss.str();

	IOperand		*newOperand = new Operand<T>(stringValue, type, precision);
	return (newOperand);
      }

    IOperand			*operator-(IOperand const& operand) const
      {
	T 			value = 0;
	double 			temp_val = 0;
	int			precision = 0;
	eOperandType		type;
	std::string 		stringValue;
	std::stringstream	ss(operand.toString());

	if (operand.getType() != INT8)
	  ss >> value;
	else
	  {
	    ss >> temp_val;
	    value = temp_val;
	  }
	temp_val = _value - value;
	type = operand.getType();
	if (_precision < operand.getPrecision())
	  {
	    precision = operand.getPrecision();
	    type = operand.getType();
	  }
	ss.clear();
	ss << temp_val;
	stringValue = ss.str();

	IOperand		*newOperand = new Operand<T>(stringValue, type, precision);
	return (newOperand);
      }

    IOperand			*operator*(IOperand const& operand) const
      {
	T 			value = 0;
	double 			temp_val = 0;
	int			precision = 0;
	eOperandType		type;
	std::string 		stringValue;
	std::stringstream	ss(operand.toString());

	if (operand.getType() != INT8)
	  ss >> value;
	else
	  {
	    ss >> temp_val;
	    value = temp_val;
	  }
	temp_val = _value * value;
	type = operand.getType();
	if (_precision < operand.getPrecision())
	  {
	    precision = operand.getPrecision();
	    type = operand.getType();
	  }

	ss.clear();
	ss << temp_val;
	stringValue = ss.str();

	IOperand		*newOperand = new Operand<T>(stringValue, type, precision);
	return (newOperand);
      }

    IOperand			*operator/(IOperand const& operand) const
      {
	T 			value = 0;
	double 			temp_val = 0;
	int			precision = 0;
	eOperandType		type;
	std::string 		stringValue;
	std::stringstream	ss(operand.toString());

	if (operand.getType() != INT8)
	  ss >> value;
	else
	  {
	    ss >> temp_val;
	    value = temp_val;
	  }

	if (value)
	  temp_val = _value / value;
	else
	  throw VMException("Divide operator: dividing by zero");

	type = operand.getType();

	if (_precision < operand.getPrecision())
	  {
	    precision = operand.getPrecision();
	    type = operand.getType();
	  }
	ss.clear();
	ss << temp_val;
	stringValue = ss.str();

	IOperand		*newOperand = new Operand<T>(stringValue, type, precision);
	return (newOperand);
      }

    IOperand			*operator%(IOperand const& operand) const
      {
	T 			value = 0;
	double 			temp_val = 0;
	int			precision = 0;
	eOperandType		type;
	std::string 		stringValue;
	std::stringstream	ss(operand.toString());
	std::ostringstream	oss;

	if (operand.getType() != INT8)
	  ss >> value;
	else
	  {
	    ss >> temp_val;
	    value = temp_val;
	  }

	if (value)
	  temp_val = fmod(_value, value);
	else
	  throw VMException("Modulo operator: dividing by zero");

	type = operand.getType();

	if (_precision < operand.getPrecision())
	  {
	    precision = operand.getPrecision();
	    type = operand.getType();
	  }
	oss << temp_val;
	stringValue = oss.str();

	IOperand		*newOperand = new Operand<T>(stringValue, type, precision);
	return (newOperand);
      }

    private:
    std::string			_stringValue;
    eOperandType		_type;
    int				_precision;
    T				_value;
  };

#endif /* !OPERAND_HH_ */
