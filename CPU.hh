/*
** CPU.hh for abstractvm in /Users/jean/Documents/epitech/cpp_abstractvm
**
** Made by jean gravier
** Login   <gravie_j@epitech.eu>
**
** Started on  Tue Feb  18 17:42:56 2014 jean gravier
// Last update Tue Feb 25 12:40:55 2014 Fritsch harold
*/

#ifndef CPU_HH_
# define CPU_HH_
# include <iostream>
# include "IOperand.hh"
# include "Memory.hh"

class		CPU
  {
    public:
    CPU();
    ~CPU();

    void	add(Memory *) const;
    void	substract(Memory *) const;
    void	multiply(Memory *) const;
    void	divide(Memory *) const;
    void	modulo(Memory *) const;
    void	push(Memory *, IOperand *) const;
    void	assert(Memory *, IOperand *) const;
    void	pop(Memory *) const;
    void	dump(Memory *) const;
    void	print(Memory *) const;
    IOperand	*getOperand(Memory *) const;
  };

#endif /* !CPU_HH_ */
