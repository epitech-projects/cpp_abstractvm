##
## Makefile for abstractvm in /Users/jean/Documents/epitech/cpp_abstractvm
##
## Made by jean gravier
## Login   <gravie_j@epitech.eu>
##
## Started on  Mon Feb  17 18:43:38 2014 jean gravier
## Last update Thu Feb 27 14:59:01 2014 Jean Gravier
##

NAME			=	avm

CC 			=	g++

RM 			=	rm -f

SRCS			=	main.cpp \
				IO.cpp \
				CPU.cpp \
				Memory.cpp \
				Chipset.cpp \
				VMException.cpp

OBJS			=	$(SRCS:.cpp=.o)

CPPFLAGS		+=	-Wall -Wextra

$(NAME): $(OBJS)
	$(CC) $(SRCS) -o $(NAME)

all: $(NAME)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: clean fclean all