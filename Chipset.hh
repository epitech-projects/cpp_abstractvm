/*
** Chipset.hh for abstractvm in /Users/jean/Documents/epitech/cpp_abstractvm
**
** Made by jean gravier
** Login   <gravie_j@epitech.eu>
**
** Started on  Mon Feb  24 19:41:59 2014 jean gravier
** Last update Mon Feb  24 19:41:59 2014 jean gravier
*/

#ifndef CHIPSET_HH
# define CHIPSET_HH
# include <string>
# include "IOperand.hh"

class 			Chipset
  {
    public:
    Chipset();
    ~Chipset();

    IOperand		*createOperand(eOperandType, std::string const&);

    private:
    IOperand		*createInt8(std::string const& value);
    IOperand		*createInt16(std::string const& value);
    IOperand		*createInt32(std::string const& value);
    IOperand		*createFloat(std::string const& value);
    IOperand		*createDouble(std::string const& value);
  };

typedef	IOperand *(Chipset::*func_ptr)(std::string const&);

#endif /* !CHIPSET_HH */