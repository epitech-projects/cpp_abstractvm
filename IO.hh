//
// IO.hh for  in /home/fritsc_h/projets/cpp_abstractvm
// 
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
// 
// Started on  Thu Feb 20 18:36:42 2014 Fritsch harold
// Last update Sat Feb 22 15:45:53 2014 Fritsch harold
//

#ifndef IO_HH_
# define IO_HH_

# include <fstream>
# include <string>
# include <vector>
# include <map>
# include "IOperand.hh"
# include "Memory.hh"
# include "CPU.hh"

class									IO
{
public:
  IO(Memory *, CPU *);
  ~IO();
  void									open(const char *);
  void									close();
  std::string								getLine();
  std::string								epur(std::string, std::string);
  void									parse(std::string);
  void									readStdin();
  void									readFile();
  bool									eof();
  eOperandType								stringToEnum(std::string);

private:
  Memory								*_mem;
  CPU									*_cpu;
  std::ifstream								_file;
  bool									_filetype;
  std::vector<std::string>						_buff;
  std::map<std::string, eOperandType>					_stringEnum;
  std::map<std::string, void (CPU::*)(Memory *) const>			_func;
  std::map<std::string, void (CPU::*)(Memory *, IOperand *) const>	_funcArg;
};

#endif /* !PARSER_HH_ */
