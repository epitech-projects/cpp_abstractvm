/*
** Memory.hh for abstractvm in /Users/jean/Documents/epitech/cpp_abstractvm
**
** Made by jean gravier
** Login   <gravie_j@epitech.eu>
**
** Started on  Tue Feb  18 17:21:48 2014 jean gravier
** Last update Tue Feb  18 17:23:48 2014 jean gravier
*/

#ifndef MEMORY_HH_
# define MEMORY_HH_
# include <list>
# include "IOperand.hh"

class		Memory
  {
    public:
    Memory();
    ~Memory();

    std::list<IOperand *> _operands;
  };
#endif /* !MEMORY_HH_ */
