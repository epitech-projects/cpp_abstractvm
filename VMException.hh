//
// VMException.hh for abstractvm in /home/gravie_j/Documents/projets/cpp_abstractvm
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Feb 27 14:45:06 2014 Jean Gravier
// Last update Thu Feb 27 15:20:27 2014 Jean Gravier
//

#ifndef VMEXCEPTION_HH_
#define VMEXCEPTION_HH_

#include <string>

class		VMException: public std::exception
{
public:
  VMException(std::string const& = "error");
  ~VMException() throw();

  virtual const char	*what() const throw();

private:
  std::string	_message;
};

#endif /* !VMEXCEPTION_HH_ */
