//
// IOperand.hh for  in /home/fritsc_h/projets/cpp_abstractvm
//
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
//
// Started on  Mon Feb 17 17:45:44 2014 Fritsch harold
// Last update Mon Feb 17 18:51:04 2014 Fritsch harold
//

#ifndef IOPERAND_HH_
# define IOPERAND_HH_
# include <string>

typedef	enum			eOperandType
  {
    INT8,
    INT16,
    INT32,
    FLOAT,
    DOUBLE
  }				eOperandType;

class				IOperand
  {
    public:
    virtual			~IOperand() {}

    virtual std::string	const&	toString() const = 0;
    virtual int			getPrecision() const = 0;
    virtual eOperandType	getType() const = 0;

    virtual IOperand		*operator+(IOperand const&) const = 0;
    virtual IOperand		*operator-(IOperand const&) const = 0;
    virtual IOperand		*operator*(IOperand const&) const = 0;
    virtual IOperand		*operator/(IOperand const&) const = 0;
    virtual IOperand		*operator%(IOperand const&) const = 0;
  };

#endif /* !IOPERAND_HH_ */
