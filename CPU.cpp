/*
** CPU.cpp for abstractvm in /Users/jean/Documents/epitech/cpp_abstractvm
**
** Made by jean gravier
** Login   <gravie_j@epitech.eu>
**
** Started on  Fri Feb  21 18:11:53 2014 jean gravier
// Last update Sat Mar  1 15:29:16 2014 Jean Gravier
*/

#include <list>
#include <sstream>
#include "types.hh"
#include "CPU.hh"
#include "Memory.hh"
#include "IOperand.hh"
#include "VMException.hh"

CPU::CPU() {}

CPU::~CPU() {}

void			CPU::add(Memory *memory) const
  {
    IOperand 		*first = this->getOperand(memory);
    this->pop(memory);

    IOperand 		*second = this->getOperand(memory);
    this->pop(memory);

    this->push(memory, *first + *second);
  }

void			CPU::substract(Memory *memory) const
  {
    IOperand 		*first = this->getOperand(memory);
    this->pop(memory);

    IOperand 		*second = this->getOperand(memory);
    this->pop(memory);

    this->push(memory, *first - *second);
  }

void			CPU::multiply(Memory *memory) const
  {
    IOperand 		*first = this->getOperand(memory);
    this->pop(memory);

    IOperand 		*second = this->getOperand(memory);
    this->pop(memory);

    this->push(memory, *first * *second);
  }

void			CPU::divide(Memory *memory) const
  {
    IOperand 		*first = this->getOperand(memory);
    this->pop(memory);

    IOperand 		*second = this->getOperand(memory);
    this->pop(memory);

    this->push(memory, *first / *second);
  }

void			CPU::modulo(Memory *memory) const
  {
    IOperand 		*first = this->getOperand(memory);
    this->pop(memory);

    IOperand 		*second = this->getOperand(memory);
    this->pop(memory);

    this->push(memory, *first % *second);
  }

void			CPU::push(Memory *memory, IOperand *operand) const
  {
    memory->_operands.push_front(operand);
  }

void			CPU::assert(Memory *memory, IOperand *operand) const
  {
    IOperand	*top;

    top = this->getOperand(memory);
    if (operand->toString() != top->toString())
      throw VMException("Assert: different values");
  }

void			CPU::pop(Memory *memory) const
{
  if (memory->_operands.size())
    memory->_operands.pop_front();
  else
    throw VMException("Pop: empty list");
}

void					CPU::dump(Memory *memory) const
  {
    IOperand				*operand;
    std::list<IOperand *>::iterator 	it;

    it = memory->_operands.begin();
    while (it != memory->_operands.end())
      {
	operand = *it;
	std::cout << operand->toString() << std::endl;
	++it;
      }
  }

void			CPU::print(Memory *memory) const
  {
    IOperand	*operand;

    operand = this->getOperand(memory);
    if (operand->getType() == INT8)
      {
	std::stringstream ss(operand->toString());
	int			temp = 0;
	char			chr;

	ss >> temp;
	chr = temp;
	std::cout << chr;
      }
    else
      throw VMException("Print: not Int8");
  }


IOperand		*CPU::getOperand(Memory *memory) const
  {
    if (memory->_operands.size())
      {
	IOperand 	*operand = memory->_operands.front();

	return (operand);
      }
    else
      throw VMException("Empty list");
    return (NULL);
  }
