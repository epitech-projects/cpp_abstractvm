//
// types.hh for  in /home/fritsc_h/projets/cpp_abstractvm
// 
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
// 
// Started on  Tue Feb 25 12:42:29 2014 Fritsch harold
// Last update Tue Feb 25 12:43:34 2014 Fritsch harold
//

#ifndef TYPES_HH_
# define TYPES_HH_

#ifndef _INT8_T
#define _INT8_T
typedef __signed char           int8_t;
#endif

#ifndef _INT16_T
#define _INT16_T
typedef short                   int16_t;
#endif

#ifndef _INT32_T
#define _INT32_T
typedef int                     int32_t;
#endif

#endif /* !TYPES_HH_ */
