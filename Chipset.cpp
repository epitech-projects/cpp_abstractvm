/*
** Chipset.cpp for abstractvm in /Users/jean/Documents/epitech/cpp_abstractvm
**
** Made by jean gravier
** Login   <gravie_j@epitech.eu>
**
** Started on  Mon Feb  24 19:42:10 2014 jean gravier
// Last update Tue Feb 25 12:44:45 2014 Fritsch harold
*/

#include <string>
#include "types.hh"
#include "Chipset.hh"
#include "IOperand.hh"
#include "Operand.hh"

Chipset::Chipset()
  {
  }

Chipset::~Chipset()
  {
  }

IOperand		*Chipset::createOperand(eOperandType type, std::string const& value)
  {
    func_ptr		func_tab[5];

    func_tab[INT8] = &Chipset::createInt8;
    func_tab[INT16] = &Chipset::createInt16;
    func_tab[INT32] = &Chipset::createInt32;
    func_tab[FLOAT] = &Chipset::createFloat;
    func_tab[DOUBLE] = &Chipset::createDouble;

    return ((this->*func_tab[type])(value));
  }

IOperand		*Chipset::createInt8(std::string const& value)
  {
    int		precision = 0;
    if (value.find_first_of(".") != std::string::npos)
      precision = value.length() - value.find_first_of(".") - 1;

    IOperand	*operand = new Operand<int8_t>(value, INT8, precision);
    return (operand);
  }
IOperand		*Chipset::createInt16(std::string const& value)
  {
    int		precision = 0;
    if (value.find_first_of(".") != std::string::npos)
      precision = value.length() - value.find_first_of(".") - 1;

    IOperand	*operand = new Operand<int16_t>(value, INT16, precision);
    return (operand);
  }
IOperand		*Chipset::createInt32(std::string const& value)
  {
    int		precision = 0;
    if (value.find_first_of(".") != std::string::npos)
      precision = value.length() - value.find_first_of(".") - 1;

    IOperand	*operand = new Operand<int32_t>(value, INT32, precision);
    return (operand);
  }
IOperand		*Chipset::createFloat(std::string const& value)
  {
    int		precision = 0;
    if (value.find_first_of(".") != std::string::npos)
      precision = value.length() - value.find_first_of(".") - 1;

    IOperand	*operand = new Operand<float>(value, FLOAT, precision);

    return (operand);
  }
IOperand		*Chipset::createDouble(std::string const& value)
  {
    int		precision = 0;
    if (value.find_first_of(".") != std::string::npos)
      precision = value.length() - value.find_first_of(".") - 1;

    IOperand	*operand = new Operand<double>(value, DOUBLE, precision);
    return (operand);
  }
