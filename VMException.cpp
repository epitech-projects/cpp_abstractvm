//
// VMException.cpp for abstractvm in /home/gravie_j/Documents/projets/cpp_abstractvm
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Thu Feb 27 14:45:10 2014 Jean Gravier
// Last update Thu Feb 27 15:18:19 2014 Jean Gravier
//

#include <string>
#include "VMException.hh"

VMException::VMException(std::string const& message): _message(message)
{

}

VMException::~VMException() throw()
{

}

const char	*VMException::what() const throw()
{
  return (this->_message.c_str());
}
