//
// main.cpp for  in /home/fritsc_h/projets/cpp_abstractvm
//
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
//
// Started on  Thu Feb 20 11:51:52 2014 Fritsch harold
// Last update Fri Feb 28 15:12:48 2014 Jean Gravier
//

#include <ios>
#include <iostream>
#include <fstream>
#include <exception>
#include <cstdlib>
#include "IO.hh"
#include "Operand.hh"
#include "IOperand.hh"
#include "CPU.hh"
#include "Memory.hh"
#include "VMException.hh"

int			main(int ac, char **av)
{
  Memory		*memory = new Memory();
  CPU			*cpu = new CPU();
  IO			parse(memory, cpu);
  std::string		line;
  std::string		stdin = "/dev/stdin";
  int			i = 1;

  try
    {
      if (ac > 1)
	while (i < ac)
	  {
	    parse.open(av[i]);
	    parse.readFile();
	    parse.close();
	    ++i;
	  }
      else
	{
	  parse.open(stdin.data());
	  parse.readStdin();
	}
      parse.close();

      delete memory;
      delete cpu;


    }
  catch (std::exception &e)
    {
      std::cerr << "Error: " << e.what() << std::endl;
      abort();
    }
  return (0);
}
