//
// IO.cpp for  in /home/fritsc_h/projets/cpp_abstractvm
//
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
//
// Started on  Fri Feb 28 19:13:12 2014 Fritsch harold
// Last update Sat Mar  1 14:48:10 2014 Jean Gravier
//

#include <iostream>
#include <fstream>
#include <istream>
#include <string>
#include <algorithm>
#include "IOperand.hh"
#include "Operand.hh"
#include "Chipset.hh"
#include "IO.hh"
#include "CPU.hh"
#include "VMException.hh"

IO::IO(Memory *mem, CPU *cpu)
  {
    _mem = mem;
    _cpu = cpu;
    _stringEnum["int8"] = INT8;
    _stringEnum["int16"] = INT16;
    _stringEnum["int32"] = INT32;
    _stringEnum["float"] = FLOAT;
    _stringEnum["double"] = DOUBLE;
    _func["add"] = &CPU::add;
    _func["sub"] = &CPU::substract;
    _func["mul"] = &CPU::multiply;
    _func["div"] = &CPU::divide;
    _func["mod"] = &CPU::modulo;
    _func["pop"] = &CPU::pop;
    _func["dump"] = &CPU::dump;
    _func["print"] = &CPU::print;
    _funcArg["push"] = &CPU::push;
    _funcArg["assert"] = &CPU::assert;
  }

IO::~IO()
  {
    _stringEnum.clear();
    _func.clear();
    _funcArg.clear();
  }

void		IO::open(const char *filename)
{
  _file.open(filename, std::ios::in);
}

std::string	IO::getLine()
  {
    std::string	line;

    std::getline(_file, line);
    return (line);
  }

void		IO::readStdin()
{
  std::string	line;
  std::vector<std::string>::iterator	it;
  size_t	i;

  while ((line = epur(getLine(), " ")) != ";;")
    {
      while ((i = line.find_first_of("\t", i)) != std::string::npos)
	line.replace(i, 1, " ");
      line = epur(line, " ");
      if (line != "")
	_buff.push_back(line);
    }
  it = _buff.begin();
  while (it != _buff.end())
    {
      parse(*it);
      ++it;
    }
}

void		IO::readFile()
{
  std::string	line;
  size_t	i;

  while (!eof())
    {
      line = getLine();
      while ((i = line.find_first_of("\t", i)) != std::string::npos)
	line.replace(i, 1, " ");
      line = epur(epur(line, " "), "\t");
      if (line == "exit")
	return ;
      parse(line);
    }
  throw VMException("not terminating with instruction exit");
}

void		IO::parse(std::string line)
  {
    std::string	instruction; //va contenir l'instruction (push, mod add etc...)
    std::string	parameter = ""; //va contenir la chaine temporaire qui servira à créer ensuite type et value
    std::string	type; //va contenir le type de la valeur d'un push ou assert
    std::string	value; //va contenir la valeur lors d'un push ou assert

    if (line.length() && (line != "") && (line[0] != ';')) //check si ligne vide ou commentée
      {
	if (line.find_first_of(";", 0) != std::string::npos)
	  line.erase(line.find_first_of(";", 0), line.length() - line.find_first_of(";", 0));
	if ((std::count(line.begin(), line.end(), '(') > 1) || (std::count(line.begin(), line.end(), ')') > 1) || (std::count(line.begin(), line.end(), ' ') > 1)) //check le bon nombre de parenthèses et d'espaces
	  throw VMException("incorrect line format");
	instruction = line.substr(0, line.find_first_of(" ", 0));
	if (instruction.length() != line.length())
	  {
	    parameter = line.substr(instruction.length() + 1, line.length() - instruction.length() - 1);
	    type = parameter.substr(0, parameter.find_first_of("(", 0));
	    value = parameter.substr(parameter.find_first_of("(", 0) + 1, parameter.find_first_of(")", 0) - parameter.find_first_of("(", 0) - 1);
	    if (type == "float" || type == "double")
	      {
		if ((value.find_first_not_of("0123456789.-")) != std::string::npos)
		  throw VMException("invalid double/float value");
	      }
	    else
	      if ((value.find_first_not_of("0123456789-")) != std::string::npos)
		throw VMException("invalid int value");
	  }

	//check dans la map des fonctions qui prennent et qui prennent pas d'arguments
	if (_func[instruction] != 0)
	  {
	    (_cpu->*(_func[instruction]))(_mem);
	  }
	else if (_funcArg[instruction] != 0)
	  {
	    Chipset	chipset;
	    IOperand	*operand = chipset.createOperand(stringToEnum(type), value);

	    (_cpu->*(_funcArg[instruction]))(_mem, operand);
	  }
	else
	  {
	    std::string	message("invalid instruction: ");
	    throw VMException(message + instruction);
	  }
      }
  }

std::string	IO::epur(std::string str, std::string to_find)
  {
    std::string	result("");
    size_t	i = 0;

    while ((i = str.find_first_not_of(to_find, i)) != std::string::npos)
      {
	result += str.substr(i, str.find_first_of(to_find, i) - i) + " ";
	i = str.find_first_of(to_find, i);
      }
    if (result.length())
      result.erase(result.length() - 1, 1);
    return (str = result);
  }

eOperandType	IO::stringToEnum(std::string str)
  {
    if (str == "")
      return ((eOperandType)0);
    else if ((_stringEnum[str] == 0) && (str != "int8"))
      {
	std::string message("Invalid type in stringToEnum: ");
	throw VMException(message + str);
      }
    return (_stringEnum[str]);
  }

bool		IO::eof()
{
  return (_file.eof());
}

void		IO::close()
{
  _file.close();
}
